package com.example.firebasecrud

import android.app.AlertDialog
import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.example_item.view.*


class RecyclerViewAdapter  (private val context:Context, private val heroList: List<Hero>) : RecyclerView.Adapter<RecyclerViewAdapter.ExampleViewHolder>() {
    //here attaching layout of item to be recycled
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewAdapter.ExampleViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.example_item, parent, false)
        return ExampleViewHolder(itemView)
    }

    //declaring item size of my recyclerview
    override fun getItemCount() = heroList.size

    override fun onBindViewHolder(holder: RecyclerViewAdapter.ExampleViewHolder, position: Int) {
        //getting position of each item in my recyclerview
        val currentItem = heroList[position]
        //setting details to recycled item view
        Glide.with(context)
            .load(currentItem.imageHero)
            .into(holder.imageView)
        holder.textView1.text = currentItem.name
        holder.textView2.text = currentItem.rating.toString()

        //variables to store update details
        val nameHero: String = currentItem.name
        val heroRating: Int = currentItem.rating
        val heroImage: String = currentItem.imageHero
        val id: String = currentItem.id
        //when item is clicked launch the update and delete dialog
        holder.cardClick.setOnClickListener {
            updateandDeleteDialog(id,nameHero, heroRating,heroImage)
//            Toast.makeText(context,"hey",Toast.LENGTH_LONG).show()

        }

    }

    //update and delete
    private fun updateandDeleteDialog(id:String, nameHero: String, heroRating:Int, heroImage: String ) {
        //raising a dialog
        val dialogBuilder = AlertDialog.Builder(context)
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        //attaching the dialog interface
        val dialogView = inflater.inflate(R.layout.update_hero, null)
        dialogBuilder.setView(dialogView)
        //view identification
        val editName: EditText = dialogView.findViewById(R.id.editName)
        val ratings: RatingBar = dialogView.findViewById(R.id.ratingUpdate)
        val btnUpdate: Button = dialogView.findViewById(R.id.btnUpdate)
        val btnDelete: Button = dialogView.findViewById(R.id.btnDelete)
        //title to the alert
        dialogBuilder.setTitle("Update Hero")
        //create and show
        val dialog = dialogBuilder.create()
        dialog.show()
        //btn update when clicked will pass id and details to update to the method update hero
        btnUpdate.setOnClickListener{
                //capture input
                val newHeroName = editName.getText().toString().trim()
                val newHeroRating = ratings.rating.toInt()
                if (TextUtils.isEmpty(newHeroName))
                {
                    Toast.makeText(context, "hero name cannot be empty", Toast.LENGTH_SHORT).show()
                }
                else
                {
                    updateHero(id,newHeroName, newHeroRating,heroImage)
                    dialog.dismiss()
                }
            }
        //btn delete when clicked will pass id of record to method deleteHero()
        btnDelete.setOnClickListener{
                deleteHero(id)
                dialog.dismiss()
            }
    }

    //method to update record
    private fun updateHero(id: String, newHeroName: String, newHeroRating: Int,heroImage: String) {
        //fetching record reference according to id
        val databaseReference = FirebaseDatabase.getInstance().getReference("heroes").child(id)
        //uploading new details
        val heroModel = Hero(id,newHeroName,newHeroRating,heroImage)
        databaseReference.setValue(heroModel)

    }
    //deleting record based on record id
    private fun deleteHero(id: String) {
        //create ref to db
        val databaseReference = FirebaseDatabase.getInstance().getReference("heroes").child(id)
        databaseReference.removeValue()


    }

    //view holder class for view identification of recycled items widgets : using android extensions to reference the id
    class ExampleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView: ImageView = itemView.image_view
        val textView1: TextView = itemView.text_view_1
        val textView2: TextView = itemView.text_view_2
        val cardClick: CardView = itemView.cardClick
    }

    }