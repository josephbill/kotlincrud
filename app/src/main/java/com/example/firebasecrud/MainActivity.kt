package com.example.firebasecrud


import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.webkit.MimeTypeMap
import android.widget.*
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.firebasecrud.R
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.*
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException
import java.util.*

class MainActivity : AppCompatActivity() {

    private val PICK_IMAGE_REQUEST = 71
    private var filePath: Uri? = null
    private var firebaseStore: FirebaseStorage? = null
    private var storageReference: StorageReference? = null
    private var databaseReference: DatabaseReference? = null
    lateinit var editName: EditText
    lateinit var ratings: RatingBar
    lateinit var submit: Button
    lateinit var imageClick: ImageView
    private val RequestCode = 438

    //declaring a mutable list to store my list of heroes
    lateinit var heroList: MutableList<Hero>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        firebaseStore = FirebaseStorage.getInstance()
        storageReference = FirebaseStorage.getInstance().reference
        databaseReference = FirebaseDatabase.getInstance().getReference("heroes")

        editName = findViewById(R.id.name)
        ratings = findViewById(R.id.rating)
        submit = findViewById(R.id.btnsave)
        imageClick = findViewById(R.id.imageUpload)



        //uploading my image
        imageClick!!.setOnClickListener{
            pickImage()
        }

        submit!!.setOnClickListener{
            saveRating()
        }

        btnRecycler.setOnClickListener {
            val intent = Intent(this@MainActivity,RecyclerViewActivity::class.java)
            startActivity(intent)
        }


        //my heroes list
        heroList = mutableListOf()

    }

    private fun pickImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            //checking if image was picked or not
            if(data == null || data.data == null){
                return
            }
            //saving the image path
            filePath = data.data
            //display image on imageview
            try {
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
                //referencing imageview based off id : android extensions
                imageUpload.setImageBitmap(bitmap)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }



    private fun saveRating(){
        //checking if file path has data before doing the upload
        if(filePath != null){
            //getting the text input
            val username = editName.text.toString().trim()
            //validating entry
            if (username.isEmpty()){
                editName.error = "please enter a name to rate"
                return
            }

            //creating a storage reference in firebase storage where the images will be stored  : here the path is in a folder called uploads
            // + the filename captured by this tag : UUID.randomUUID().toString()"
            val ref = storageReference?.child("uploads/" + UUID.randomUUID().toString())
            //setting an upload task reference : the info stored in this variable is basically the upload process based on above reference : put the file
            //saved in the filePath!! reference the !! are null checks : read on null operators in kotlin
            val uploadTask = ref?.putFile(filePath!!)
            //checking on progress of upload
            val urlTask = uploadTask?.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                return@Continuation ref.downloadUrl
            })?.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    //using the oncomplete listener from firebase we can now check if the upload is successful
                    //and then also pick the download uri : download address for the image saved
                    val downloadUri = task.result
                    //if upload is successful then we can now upload above uri to firebase real time database
                    Log.d("searchUrl"," download url is " + downloadUri.toString())
                    //
                    val heroId = databaseReference?.push()?.key
                    //my hero object //we need to add the image ref. as a string thus the tag , downloadUri.toString
                    //make sure u added this to the constructor of the data class Hero
                     val hero = heroId?.let { Hero(it,username,ratings.rating.toInt(),downloadUri.toString())}
                    //pushing my hero object to my database reference , surrounding with null check to prevent errors
                   //this will save our hero to our firebase database
                      if (heroId != null) {
                          databaseReference?.child(heroId)?.setValue(hero)?.addOnCompleteListener {
                              Toast.makeText(
                                  applicationContext,
                                  "Hero rated successfully",
                                  Toast.LENGTH_LONG
                              ).show()
                          }
                      }

                } else {
                    // Handle failures //e.g //
                    Toast.makeText(applicationContext," Error occurred , check internet connection",Toast.LENGTH_LONG).show()
                }
            }?.addOnFailureListener{
                   //here u can get actual error from firebase
                   val messageError = it.message
                   Toast.makeText(applicationContext," Error is " + messageError,Toast.LENGTH_LONG).show()
            }
        }else{
            Toast.makeText(this, "Please Upload an Image", Toast.LENGTH_SHORT).show()
        }
    }

}