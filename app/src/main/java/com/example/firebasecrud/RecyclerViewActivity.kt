package com.example.firebasecrud

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_recycler_view.*

class RecyclerViewActivity : AppCompatActivity() {
    private var databaseReference: DatabaseReference? = null
   lateinit var recycler: RecyclerView
    //declaring a mutable list to store my list of heroes
    lateinit var heroList: MutableList<Hero>
    lateinit var heroAdapter: HeroAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_view)
         //intializing ref to  firebase db to pull data from
        databaseReference = FirebaseDatabase.getInstance().getReference("heroes")
        //my heroes list
        heroList = mutableListOf()


        databaseReference!!.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(@NonNull snapshot: DataSnapshot) {
                // this method is call to get the realtime
                // updates in the data.
                // this method is called when the data is
                // changed in our Firebase console.
                // below line is for getting the data from
                // snapshot of our database.

                //here checking if data exists in firebase node using the snapshot tag of the DataSnapshot which saves the records
                if(snapshot!!.exists()){
                    //updating our mutable list everytime a change is effected on node
                    heroList.clear()
                    //using for loop to iterate over the records in the node
                    for (h in snapshot.children){
                        //adding records to my model class
                        val bal = h.getValue(Hero::class.java)
                        //adding details to my mutable list
                        heroList?.add(bal!!)
                    }

                    //tagging my adapter for communication between my data class and this activity
                    val adapter = RecyclerViewAdapter(this@RecyclerViewActivity!!,heroList)
                    //setting the adapter for the recyclerView
                    recycler_view?.setAdapter(adapter)

                }

            }
            override fun onCancelled(@NonNull error: DatabaseError) {
                // calling on cancelled method when we receive
                // any error or we are not able to get the data.
                Toast.makeText(this@RecyclerViewActivity, "Fail to get data.", Toast.LENGTH_SHORT).show()
            }
        })


        //setting the layout orientation for my recycler view
        recycler_view.layoutManager = LinearLayoutManager(this)
        //setting size to data size
        recycler_view.setHasFixedSize(true)
    }
}