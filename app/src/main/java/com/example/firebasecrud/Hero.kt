package com.example.firebasecrud

import android.net.Uri
import com.google.android.gms.tasks.Task

class Hero(val id: String, val name: String, val rating: Int, val imageHero: String){
    //my blank constructor
    constructor() : this("","",0,""){
    }
}